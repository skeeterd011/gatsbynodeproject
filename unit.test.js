const request = require('supertest');
const app = require('../app');

describe('Static website', () => {
  test('renders homepage', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toContain('Welcome to my website!');
  });

  test('renders about page', async () => {
    const response = await request(app).get('/about');
    expect(response.status).toBe(200);
    expect(response.text).toContain('This is the about page.');
  });

  test('returns 404 for nonexistent page', async () => {
    const response = await request(app).get('/not-found');
    expect(response.status).toBe(404);
  });
});
