# Use an official Node runtime as a parent image
FROM node:18-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the container
COPY . .

# Build the Gatsby static website
RUN npm run build

# Serve the static website using Nginx
FROM nginx:alpine
COPY --from=0 /app/public /usr/share/nginx/html
